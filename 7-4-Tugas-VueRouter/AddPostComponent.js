export const AddPostComponent = {
    template: `
    <div class="row mt-4 justify-content-center">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <form @submit.prevent="$parent.addPost">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" v-model="$parent.postTitle">
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea name="content" id="content" class="form-control" v-model="$parent.postContent"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <input type="text" class="form-control" name="category" id="category" v-model="$parent.postCategory">
                        </div>
                        <button type="submit" class="btn btn-primary mt-4">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    `,
}