<?php

namespace App\Http\Controllers\API;

use App\Comment;
use App\Events\CommentStoredEvent;
use App\Http\Controllers\Controller;
// use App\Mail\CommentAuthorMail;
// use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;

        $comments = Comment::where('post_id', $post_id)->latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Comments List',
            'data' => $comments
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id
        ]);

        event(new CommentStoredEvent($comment));

        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data' => $comment
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::findOrfail($id);

        return response()->json([
            'success' => true,
            'message' => 'Comment Detail',
            'data' => $comment
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::findOrFail($id);

        if ($comment) {
            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'You are not authorized to this comment'
                ], 403);
            }

            $comment->update([
                'content' => $request->content
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data' => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        if ($comment) {
            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'You are not authorized to this comment'
                ], 403);
            }

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted'
            ], 202);
        }

        return response()->json([
            'success' => false,
            'messsage' => 'Comment Not Found'
        ], 404);
    }
}
