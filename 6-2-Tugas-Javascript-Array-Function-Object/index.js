// Soal 1
var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];

for (var i = 1; i < daftarHewan.length; i++) {
    for (var j = 0; j < i; j++) {
        if (daftarHewan[i] < daftarHewan[j]) {
            var x = daftarHewan[i];
            daftarHewan[i] = daftarHewan[j];
            daftarHewan[j] = x;
        }
    }
}

console.log(daftarHewan);
console.log();

// Soal 2
function introduce(obj) {
    return 'Nama saya ' + obj.name + ', umur saya ' + obj.age + ' tahun, alamat saya di ' + obj.address + ', dan saya punya hobby yaitu ' + obj.hobby + '!';
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming"};

var perkenalan = introduce(data);

console.log(perkenalan);
console.log();

// Soal 3
function hitung_huruf_vokal(str) {
    var vocal = str.match(/[aiueo]/gi);
    return vocal === null ? 0 : vocal.length;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2);
console.log();

// Soal 4
function hitung(num) {
    return (num * 2) - 2;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));