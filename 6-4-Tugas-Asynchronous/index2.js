// Soal 2
var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// Jawaban Soal 2

readBooksPromise(10000, books[0]).then((timeRemaining0) => {
    readBooksPromise(timeRemaining0, books[1]).then((timeRemaining1) => {
        readBooksPromise(timeRemaining1, books[2]).then((timeRemaining2) => {
            readBooksPromise(timeRemaining2, books[3]).then((timeRemaining3) => {
                console.log('')
            }).catch((error) => console.log(error))
        }).catch((error) => console.log(error))
    }).catch((error) => console.log(error))
}).catch((error) => console.log(error))


// let read = (time, books, i) => {
//     if (i < books.length) {
//         readBooksPromise(time, books[i]).then((remaining) => {
//             if (remaining > 0) {
//                 i++;
//                 read(remaining, books, i)
//             }
//         })
//     }
// }

// read(10000, books, 0);

