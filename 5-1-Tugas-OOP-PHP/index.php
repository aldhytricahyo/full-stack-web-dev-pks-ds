<?php
    trait Hewan
    {
        public $nama;
        public $darah = 50;
        public $jumlahKaki;
        public $keahlian;
        public function atraksi()
        {
            echo "$this->nama sedang $this->keahlian\n";
        }
    }
    trait Fight
    {
        public $attackPower;
        public $defencePower;
        public function serang($hewanDiserang)
        {
            echo "$this->nama sedang menyerang $hewanDiserang->nama\n";
            $this->diserang($hewanDiserang);
        }
        public function diserang($hewanDiserang)
        {
            echo "$hewanDiserang->nama sedang di serang!\n";
            return $hewanDiserang->darah -= ($this->attackPower/$hewanDiserang->defencePower);
        }
    }
    class Elang
    {
        use Hewan;
        use Fight;
        public function __construct($nama) {
            $this->jumlahKaki = 2;
            $this->keahlian = "terbang tinggi";
            $this->attackPower = 10;
            $this->defencePower = 5;
            $this->nama = $nama;
        }
        public function getInfoHewan()
        {
            echo "\n--------------------\n";
            echo "Nama: $this->nama\nKeahlian: $this->keahlian\nAttack Power: $this->attackPower\nDefence Power: $this->defencePower\nDarah: $this->darah";
            echo "\n--------------------\n";
        }
    }
    class Harimau
    {
        use Hewan;
        use Fight;
        public function __construct($nama) {
            $this->jumlahKaki = 4;
            $this->keahlian = "lari cepat";
            $this->attackPower = 7;
            $this->defencePower = 8;
            $this->nama = $nama;
        }
        public function getInfoHewan()
        {
            echo "\n--------------------\n";
            echo "Nama: $this->nama\nKeahlian: $this->keahlian\nAttack Power: $this->attackPower\nDefence Power: $this->defencePower\nDarah: $this->darah";
            echo "\n--------------------\n";
        }
    }

    $elang1 = new Elang("elang_1");
    $harimau1 = new Harimau("harimau_1");

    $elang1->atraksi();
    
    $elang1->serang($harimau1);
    $elang1->serang($harimau1);
    $elang1->serang($harimau1);
    $harimau1->getInfoHewan();
    $elang1->getInfoHewan();
